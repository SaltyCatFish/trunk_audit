VERSION="2.1"
DOCKER_IMAGE_NAME="saltycatfish/trunk-audit_v$VERSION"
REMOTE_NAME="rrl169@prod:/apps/docker_images/trunk_audit"
TEMP_FILE_NAME="image"

GREEN='\033[0;32m'
CYAN='\033[0;36m'
NC='\033[0m'

function ts(){
  echo "${GREEN}[$(date +%Y-%m-%d:%H:%M:%S)] ${NC}"
}

function log(){
  printf "$(ts)${CYAN}$1\n${NC}"
}

log "Get rid of the old shitful file if its still lingering like a drunk dial at noon"
if [ -f  $TEMP_FILE_NAME ]
  then
  rm $TEMP_FILE_NAME
fi

log "Get some fresh node modules without the devel dependencies"
rm -rf node_modules &&
# npm install --production &&

log "Build that sweet sweet docker image"
docker build --force-rm --disable-content-trust --no-cache -t $DOCKER_IMAGE_NAME . &&

log "Save that sweet sweet docker image"
docker save $DOCKER_IMAGE_NAME > $TEMP_FILE_NAME &&

log "Upload that sweet sweet docker image"
scp -p $TEMP_FILE_NAME $REMOTE_NAME &&

log "Dump that now useless saved image"
rm $TEMP_FILE_NAME
