/* globals require, process */

const
  express = require('express'),
  helmet = require('helmet'),
  compression = require('compression'),
  rules = require('./routes/rules.js'),
  log4js = require('log4js'),
  cors = require('cors'),

  app = express();

log4js.configure('config/log4js.json', { reloadSecs: 300 });
const log = log4js.getLogger();

app.use(log4js.connectLogger(log, { level: log4js.levels.INFO }));
app.use(cors());
app.use(helmet());
app.use(compression());
app.use(express.static('dist'));
app.use('/rules', rules);
app.use(function(err, req, res, next) {
  "use strict";
  //TODO something better than this
  res.status(500).send(err.stack);
});


app.get('/', function(req, res, next) {
  "use strict";
  res.sendFile('index.html');
  next();
});

app
  .set('port', process.env.PORT || 32781)
  .listen(app.get('port'), function() {
    "use strict";
    console.log(`Express started on port ${app.get('port')}: press ctrl-c to terminate`);
  });
