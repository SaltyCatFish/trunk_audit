#TDCH docker file

FROM saltycatfish/node-with-oracle:latest

WORKDIR /
COPY ./ /

RUN \
apt update && \
npm install  --production

ENV LD_LIBRARY_PATH=/opt/oracle/instantclient:$LD_LIBRARY_PATH

CMD ["npm", "start"]