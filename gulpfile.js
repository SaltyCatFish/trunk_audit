// jshint ignore: start
//
const
  gulp = require('gulp'),
  concat = require('gulp-concat'),
  sass = require('gulp-sass'),
  webserver = require('gulp-webserver'),
  minifyCss = require('gulp-minify-css'),
  jshint = require('gulp-jshint'),
  stylish = require('jshint-stylish'),
  todo = require('gulp-todo'),
  babel = require('gulp-babel'),
  sourcemaps = require('gulp-sourcemaps'),
  ngAnnotate = require('gulp-ng-annotate'),
  uglify = require('gulp-uglify'),
  plumber = require('gulp-plumber'),

  paths = {
    sass: ['./scss/**/*.scss'],
    vendor: [
      'node_modules/lodash/lodash.js', //lodash gave me issues if moves from this spot in line
      //'node_modules/@saltycatfish/nunu/dist/nunu.js',
      'node_modules/angular/angular.js',
      'node_modules/angular-animate/angular-animate.js',
      'node_modules/angular-sanitize/angular-sanitize.js',
      'node_modules/angular-touch/angular-touch.js',
      'node_modules/angular-resource/angular-resource.js',
      'node_modules/angular-route/angular-route.js',
      'node_modules/angular-material/angular-material.js',
      'node_modules/angular-aria/angular-aria.js',
      'node_modules/ng-table/bundles/ng-table.js'
    ],
    app: [
      '../nunu/dist/nunu.js',
      'src/js/app.module.js',
      'src/js/**/*.js',
      '!app/js/**/*.spec.js'
    ]
  };

gulp.task('default', ['watch']);

gulp.task('sass', (done) => {
  return gulp.src(paths.sass)
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(concat('styles.min.css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist'));
});

gulp.task('lint', () => {
  return gulp.src(paths.app)
    .pipe(jshint())
    .pipe(jshint.reporter(stylish));
});

gulp.task('watch', () => {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.app, ['lint', 'todo', 'source']);
});

gulp.task('serve', () => {
  gulp.src('dist')
    .pipe(webserver({
      livereload: true,
      host: '0.0.0.0',
      open: false,
      port: 8000
    }));
});

gulp.task('todo', () => {
  gulp.src('src/js/**/*.js')
    .pipe(todo())
    .pipe(gulp.dest('./'));
});

gulp.task('source', () => {
  return gulp.src(
      paths.app
    )
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(concat('app.min.js', { newLine: ';' }))
    .pipe(ngAnnotate({ add: true }))
    .pipe(uglify({ mangle: true }))
    .pipe(plumber.stop())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist'));
})

gulp.task('vendor', () => {
  return gulp.src(
      paths.vendor
    )
    .pipe(sourcemaps.init())
    .pipe(plumber())
    //.pipe(babel({
    //  presets: ['es2015']
    //}))
    .pipe(concat('vendor.min.js', { newLine: ';' }))
    //.pipe(ngAnnotate({ add: true }))
    //.pipe(uglify({ mangle: true }))
    .pipe(plumber.stop())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist'));
})

gulp.task('build', ['source']);
