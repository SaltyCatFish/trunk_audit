/**
 * config.js
 *
 * Holds static configurations
 */

/*globals module*/
module.exports = {
    rulesPath: './rules',
    oracle: {
      username: 'tdce',
      password: 'linesize',
      connectString: '10.209.51.19:1521/NETOPRD'
    },
    sqlite3: {
      dbName: 'audit.db'
    }
};
