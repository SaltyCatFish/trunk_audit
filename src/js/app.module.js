/*globals angular*/

(function() {
  'use strict';
  /**
   * @ngdoc overview
   * @name trunkAuditApp
   * @description
   * # trunkAuditApp
   *
   * Main module of the application.
   */
  angular
    .module('app', [
      'ngAnimate',
      'ngAria',
      'ngRoute',
      'ngMaterial',
      'ngTable'
    ])
    .config(function($routeProvider) {
      $routeProvider
        .when('/', {
          templateUrl: 'views/main.html',
          controller: 'MainCtrl',
          controllerAs: 'vm'

        })
        .when('/about', {
          templateUrl: 'views/about.html',
          controller: 'AboutCtrl',
          controllerAs: 'vm'
        })
        .otherwise({
          redirectTo: '/'
        });
    });
})();
