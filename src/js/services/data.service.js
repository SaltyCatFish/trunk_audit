/* jshint esversion: 6 */
/*global angular */

(function() {
  "use strict";

  //TODO Document this
  angular
    .module('app')
    .factory('dataService', dataService);

  dataService.$inject = ['$http', '$log'];

  function dataService($http, $log) { // jshint ignore:line
    let
      fetchRulesData,
      fetchRuleCsv,
      fetchRuleResults;


    fetchRulesData = function() {
      let url = "http://10.243.39.90:32781/rules";
      $log.info("Fetching initial rules data");
      return $http.get(url)
        .then((response) => {
          return response.data;
        })
        .catch((err) => {
          console.log(err);
        });
    };

    fetchRuleResults = function(id) {
      let url = `http://10.243.39.90:32781/rules/${id}/data`;
      $log.info(`Fetching rule results for ${id}`);
      return $http.get(url)
        .then((response) => {
          return response.data;
        })
        .catch((err) => {
          console.log(err);
        });
    };

    fetchRuleCsv = function(id) {
      let url = `http://10.243.39.90:32781/rules/${id}/csv`;
      $log.info(`Fetching csv export for ${id}`);
      return $http.get(url)
        .then((response) => {
          return response.data;
        })
        .catch((err) => {
          console.log(err);
        });
    };

    return {
      fetchRuleCsv,
      fetchRulesData,
      fetchRuleResults
    };
  }
})();
