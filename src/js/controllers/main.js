/* jshint esversion: 6 */
/*globals angular, R, NgTableParams */
(function() {
  'use strict';

  /**
   * @ngdoc function
   * @name trunkAuditApp.controller:MainCtrl
   * @description
   * # MainCtrl
   * Controller of the trunkAuditApp
   */
  angular
    .module('app')
    .controller('MainCtrl', MainCtrl);

  MainCtrl.$inject = ['$http', '$log', '$mdSidenav', 'dataService', 'NgTableParams'];

  function MainCtrl($http, $log, $mdSidenav, dataService, NgTableParams) {
    const
      expirationDays = 30;

    let
      vm = this;
    vm.activeCategory = "";
    vm.activeRuleId = "";
    vm.ruleResultsData = "";
    vm.filteredData = filteredData;

    vm.toggleLeft = toggleLeft;
    vm.fetchRuleCsv = fetchRuleCsv;
    vm.toggleLoading = toggleLoading;
    vm.getStatus = getStatus;
    vm.fetchRuleResults = fetchRuleResults;
    vm.tableParams = "";

    function fetchRuleResults(id) {
      $log.info("Fetching rule results");
      toggleLoading(id);
      dataService.fetchRuleResults(id)
        .then((data) => {
          vm.activeRuleId = id;
          vm.ruleResultsData = data.rows;
          updateTableParams();
          console.log(data);
        })
        .catch((err) => {
          $log.error(err);
        })
        .finally(() => {
          toggleLoading(id);
        });
    }

    function fetchRuleCsv(id) {
      $log.info("Fetching rule csv export");
      if (!id) {
        console.warning(`There is currently no active rule`);
        return;
      }
      toggleLoading(id);
      dataService.fetchRuleCsv(id)
        .then((data) => {
          console.log(data);
        })
        .catch((err) => {
          $log.error(err);
        })
        .finally(() => {
          toggleLoading(id);
        });
    }

    function updateTableParams() {
      vm.tableParams = new NgTableParams({}, {
        dataset: vm.ruleResultsData
      });
    }

    function toggleLoading(id) {
      let rule = R.find(R.propEq('id', id))(vm.data);
      if (!rule) {
        console.error(`Rule ${id} not found`);
        return;
      }
      rule.status = rule.status === 'loading' ? 'idle' : 'loading';
      return rule.status;
    }

    function getStatus(id) {
      let rule = R.find(R.propEq('id', id))(vm.data);
      if (!rule) {
        console.error(`Rule ${id} not found`);
        return false;
      }
      return R.prop('status', rule);
    }

    function toggleLeft() {
      $log.info("Sidebar has been toggled");
      $mdSidenav('left').toggle();
    }

    function getUniqueCategories(data) {
      $log.info(`Extracting unique categories from data`);
      const categories = R.pluck('category');
      return R.uniq(categories(data));
    }

    function filteredData(activeCategory) {
      if (!activeCategory) {
        return;
      }
      const matchCategory = R.propEq('category', activeCategory);
      return R.filter(matchCategory, vm.data);
    }

    function assignInitialStatus(rule) {
      const
        expiration = expirationDays * 1000 * 60 * 60 * 24,
        lastRun = parseInt(rule.lastReportRun),
        today = new Date().getTime(),
        isExpired = (lastRun + expiration) < today;
      rule.status = isExpired ? 'expired' : 'idle';

      return rule;
    }

    function activate() {
      $log.info(`Activation; fethching initial rules data`);
      dataService.fetchRulesData()
        .then((data) => {
          vm.data = R.map(assignInitialStatus, data);
          console.log(data);
          vm.categories = getUniqueCategories(data);
          $log.info(`Application loaded and ready`);
        })
        .catch((err) => {
          $log.error(err);
        });
    }


    activate();

  }
})();
