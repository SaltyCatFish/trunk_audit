/*globals angular*/

(function() {
  'use strict';

  /**
   * @ngdoc function
   * @name trunkAuditApp.controller:AboutCtrl
   * @description
   * # AboutCtrl
   * Controller of the trunkAuditApp
   */
  angular.module('app')
    .controller('AboutCtrl', function() {
      this.awesomeThings = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
      ];
    });
})();
