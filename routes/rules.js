/* globals require, __dirname */
(function() {
  "use strict";


  const
    express = require('express'),
    router = express.Router(),
    log4js = require('log4js'),
    path = require('path'),
    config = require('../config/app-config.js'),
    R = require('ramda'),
    AuditRule = require('./AuditRule');

  const
    log = log4js.getLogger(),
    rulesPath = path.join(__dirname, '..', config.rulesPath);
  let rules;

  router.get('/', function(req, res, next) {
    loadAllRules(rulesPath)
      .then((results) => {
        rules = results;
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.status(200).json(rules);
        next();
      });
  });

  router.get('/:id/data', function(req, res, next) {
    const
      id = req.params.id;
    if (!rules) {
      return loadAllRules(rulesPath)
        .then((rules) => {
          returnResponse(rules);
        })
        .catch((err) => {
          console.log(err);
          throw err;
        });
    } else {
      returnResponse(rules);
    }

    function returnResponse(rules) {
      const findRule = R.find(R.propEq('id', id));
      const foundRule = findRule(rules);
      if (!foundRule) {
        res.status(404).send(id + " could not be found");
      } else {
        //res.setHeader('Content-disposition', 'attachment; filename=' + exportFileName);
        //res.status(200).send(ruleObjectReturned.exportAsCsv;
        foundRule.getResultData()
          .then((data) => {
            res.status(200).send(data);
            next();
          })
          .catch((err) => {
            res.status(404).send(err);
            next();
          });
      }
    }
  });

  router.get('/:id/csv', function(req, res, next) {
    const
      id = req.params.id;
    if (!rules) {
      return loadAllRules(rulesPath)
        .then((rules) => {
          returnResponse(rules);
        })
        .catch((err) => {
          console.log(err);
          throw err;
        });
    } else {
      returnResponse(rules);
    }

    function returnResponse(rules) {
      const findRule = R.find(R.propEq('id', id));
      const
        foundRule = findRule(rules),
        exportFileName = "rule_export.csv";
      if (!foundRule) {
        res.status(404).send(id + " could not be found");
      } else {
        foundRule.getResultCsv()
          .then((data) => {
            res.setHeader('Content-disposition', 'attachment; filename=' + exportFileName);
            res.status(200).send(data);
            next();
          })
          .catch((err) => {
            res.status(404).send(err);
            next();
          });
      }
    }
  });


  function loadAllRules(path) {
    return AuditRule.loadAllRules(path)
      .then((rules) => {
        return rules;
      })
      .catch((err) => {
        throw err;
      });
  }

  module.exports = router;

})();
