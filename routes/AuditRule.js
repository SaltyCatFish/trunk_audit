/*globals require, module*/
const
  json2csv = require('json2csv'),
  crypto = require('crypto'),
  glob = require('glob'),
  oracledb = require('oracledb'),
  fs = require('fs'),
  sqlite3 = require('sqlite3').verbose(),
  sqlite3Config = require('../config/app-config.js').sqlite3,
  oracleConfig = require('../config/app-config.js').oracle,
  log4js = require('log4js'),
  db = new sqlite3.Database(`./db/${sqlite3Config.dbName}`),
  R = require('ramda');

oracledb.outFormat = oracledb.OBJECT;
oracledb.maxRows = 100;
const log = log4js.getLogger();

/**
 * Creates an instance of an Audit Rule, either from
 * static values passed in the constructor object or an
 * external file
 *
 * @constructor {object}
 */
class AuditRule {
  constructor(data) {
    log.debug(`instantiating audit rule: ${JSON.stringify(data)}`);
    if (data.filePath) {
      let file = JSON.parse(fs.readFileSync(data.filePath));
      this.title = file.title || "no title";
      this.description = file.description || "no description";
      this.category = file.category || "no category";
      this.query = file.query.join(' ') || "no query";
    } else {
      this.title = data.title || "no title";
      this.description = data.description || "no description";
      this.category = data.category || "no category";
      this.query = data.query.join(' ') || "no query";
    }
    this.id = this.getHash();
    this.results = undefined;
  }

  /**
   * Returns array of relative paths of all js
   * files recursively in a directory
   * @param  {String} rootDir
   * @return {Array of Strings}
   */
  static getAllRulePaths(rootDir) {
    "use strict";
    return new Promise((resolve, reject) => {
      glob(`${rootDir}/**/*.js`, (err, res) => {
        resolve(err ? err : res);
      });
    });
  }

  /**
   * Loads all rules from a path
   * @param  {String} path
   * @return {Array of AuditRules}
   */
  static loadAllRules(path) {
    "use strict";
    return this.getAllRulePaths(path)
      .then((pathsArray) => {
        return Promise.all(R.map(this.loadRule, pathsArray))
          .then((result) => {
            return result;
          })
          .catch((err) => {
            throw err;
          });
      })
      .catch((err) => {
        throw (err);
      });
  }

  /**
   * Loads rule from a file path
   * @param  {String} path
   * @return {AuditRule}
   */
  static loadRule(path) {
    "use strict";
    return new Promise((resolve, reject) => {
      const rule = new AuditRule({
        filePath: path
      });
      let processes = [
        rule.getLastRun(),
        rule.getLastCount()
      ];
      return Promise.all(processes)
        .then((result) => {
          rule.lastReportRun = result[0];
          rule.lastReportCount = result[1];
          resolve(rule);
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  /**
   * Runs query against database
   * @return {Promise} results object
   */
  _runQuery() {
    log.debug(`running query`);
    return oracledb.getConnection({
        user: oracleConfig.username,
        password: oracleConfig.password,
        connectString: oracleConfig.connectString
      })
      .then((connection) => {
        return connection.execute(this.query)
          .then((results) => {
            connection.close();
            this.results = results;
            let
              id = this.getHash(),
              resultCount = this.getResultCount();
            return this.updateTable(id, resultCount)
              .then(() => {
                return results;
              })
              .catch((err) => {
                throw err;
              });
          })
          .catch((err) => {
            throw err;
          });
      })
      .catch((err) => {
        throw err;
      });
  }

  /**
   * Gets query result headers
   * @return {Promise} array of headers
   */
  getResultHeaders() {
    log.debug(`getting result headers`);
    return new Promise((resolve, reject) => {
      let name = R.prop('name');
      if (!this.results) {
        return this._runQuery()
          .then((results) => {
            resolve(R.map(name, results.metaData));
          })
          .catch((err) => {
            log.error(err);
            throw err;
          });
      } else {
        resolve(R.map(name, this.results.metaData));
      }
    });
  }

  /**
   * Gets query result values
   * @return {Promise} array of values
   */
  getResultValues() {
    log.debug(`getting result values`);
    return new Promise((resolve, reject) => {
      if (!this.results) {
        return this._runQuery()
          .then((results) => {
            resolve(R.map(R.values, results.rows));
          })
          .catch((err) => {
            log.error(`getResultValues(): ${err}`);
            throw err;
          });
      } else {
        resolve(R.map(R.values, this.results.rows));
      }
    });
  }

  /**
   * Data set returned from query
   * @return {Promise} object of query result data
   */
  getResultData() {
    log.debug(`getting result data`);
    return new Promise((resolve, reject) => {
      if (!this.results) {
        return this._runQuery()
          .then((results) => {
            resolve(results);
          })
          .catch((err) => {
            throw err;
          });
      } else {
        resolve(this.results);
      }
    });
  }

  /**
   * Returns results as a csv, with header top row
   * @return {Promise}
   */
  getResultCsv() {
    log.debug(`getting result as csv`);
    return Promise.all([this.getResultHeaders(), this.getResultData()])
      .then((results) => {
        let
          headers = results[0],
          values = results[1].rows;
        return json2csv({
          fields: headers,
          data: values
        });
      });
  }

  /**
   * Number of rows returned from query
   * @return {Number}
   */
  getResultCount() {
    log.debug(`getting result count`);
    if (!this.results) {
      return this._runQuery()
        .then((results) => {
          return results.rows.length;
        })
        .catch((err) => {
          throw err;
        });
    } else {
      return this.results.rows.length;
    }
  }

  /**
   * Creates hash from combo of title, query, and description
   * @return {String} Hash
   */
  getHash() {
    log.debug(`getting rule hash id`);
    const string = this.title + this.query + this.description;
    return crypto.createHash('md5').update(string).digest('hex');
  }

  /**
   * Creates the reports DB table it it does not already
   * exist
   * @return {Promise}
   */
  createTable() {
    log.debug(`creating table (if it does not exist)`);
    return new Promise((resolve, reject) => {
      const query = `
        CREATE TABLE IF NOT EXISTS reports
        (
          id TEXT,
          last_run INTEGER NOT NULL,
          last_count INTEGER NOT NULL
        );
        CREATE UNIQUE idx_id ON positions (id)
      `;
      db.run(query, [], (err) => {
        if (err) { console.err(err); }
        resolve(err ? err : true);
      });
    });
  }

  /**
   * Updates the DB with the most recent timestamp and last run
   * count`
   * @param  {String}  id        rules hash name (see getHash())
   * @param  {Integer} lastCount the last number of rows returned by report
   * @return {Promise}
   */
  updateTable(id, lastCount) {
    log.debug(`updating rule: ${id} with last count of ${lastCount}`);
    return this.createTable()
      .then(() => {
        const
          stmt = db.prepare("INSERT INTO reports (id, last_run, last_count) VALUES (?, ?, ?);"),
          timestamp = new Date().getTime();
        stmt.run(id, timestamp, lastCount);
        stmt.finalize();
      })
      .catch((err) => {
        if (err) { console.err(err); }
        throw err;
      });
  }

  /**
   * Gets the last run timestamp of the rule
   * @return {Promise}
   */
  getLastRun() {
    log.debug(`getting date of last run`);
    return new Promise((resolve, reject) => {
      const
        id = this.getHash(),
        query = `SELECT last_run from reports WHERE id=(?) ORDER BY last_run desc`;
      db.get(query, [id], (err, row) => {
        if (!row) {
          this._runQuery()
            .then(() => {
              const now = new Date().getTime();
              resolve(now);
            })
            .catch((err) => {
              resolve(err);
            });
        } else {
          resolve(err ? err : row.last_run);
        }
      });

    });
  }

  /**
   * Gets the last run result count of the rule
   * @return {Promise}
   */
  getLastCount() {
    log.debug(`getting result count from last run`);
    return new Promise((resolve, reject) => {
      const
        id = this.getHash(),
        query = `SELECT last_count from reports WHERE id=(?) ORDER BY last_run desc`;
      db.get(query, [id], (err, row) => {
        if (!row) {
          this._runQuery()
            .then(() => {
              const now = new Date().getTime();
              resolve(now);
            })
            .catch((err) => {
              resolve(err);
            });
        } else {
          resolve(err ? err : row.last_count);
        }
      });
    });
  }


}
module.exports = AuditRule;
