{
    "title": "SFG Traffic Rule",
    "description": "SFG Traffic Rule",
    "category": "Special",
    "query": ["SELECT *",
        "FROM ne_comps",
        "JOIN trunkgroup",
        "ON TGSN       = COMPTGSN",
        "WHERE char_parm9 in ('SFG')",
        "AND CHAR_PARM25 IS NULL",
        "AND STATUS NOT IN ('previous', 'suspect')",
        "AND 'TRUE' = (CASE",
        "WHEN status = 'used' AND ne_comps.inservice_equip > 0 THEN 'TRUE' ELSE 'FALSE'",
        "END)",
        "AND ID NOT IN",
        "(SELECT ID",
        "FROM NE_COMPS n",
        "JOIN trunkgroup t",
        "ON t.TGSN = n.COMPTGSN",
        "WHERE char_parm9 in ('SFG')",
        "AND CHAR_PARM25 IS NULL",
        "AND char_parm9       IN ('SFG')",
        "AND TRIM(CLLI_A) IS NOT NULL",
        "AND TRIM(CLLI_Z) IS NOT NULL",
        "AND TRIM(TRAFFIC_CLASS) IS NULL",
        "AND TRIM(TRAFFIC_USE) IS NULL",
        "AND TRIM(MODIFIER) IS NULL",
        "AND STATUS IN ('used', 'new')",
        "AND 'TRUE' = (CASE",
        "WHEN STATUS = 'used' AND n.INSERVICE_EQUIP > 0 THEN 'TRUE'",
        "WHEN STATUS = 'new' THEN 'TRUE'",
        "ELSE 'FALSE'",
        "END)",
        ")"
    ]
}
